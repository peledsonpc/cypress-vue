describe('Testando formulário', () => {
  it('Acessando rota de formulário', () => {
    cy.visit('/form')
    cy.get('button').should('be.visible')
  })

  it('Clicando no botão', () => {
    cy.get('button').click()
  })

  it('Mensagem de erro', () => {
    cy.get('h1')
      .contains('Preencha os campos')
      .should('have.class', 'validation-error')
  })

  it('Preenche campo nome', () => {
    cy.get('input[data-test="nome')
      .type('Pedro Barros')
  })

  it('Preenche campo email', () => {
    cy.get('input[data-test="email')
      .type('pedro@email.com')
  })

  it('Clicando no botão', () => {
    cy.get('button').click()
  })

  it('Mensagem de sucesso', () => {
    cy.get('h1').contains('Sucesso')
  })
})
